import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { formatDiagnosticsWithColorAndContext } from 'typescript'

export const useLoadingStore = defineStore('loading', () => {
    const isLoading = ref(false)
    const doLoad = () => {
        isLoading.value = true
    }
    const finsih = () => {
        isLoading.value = false
    }

    return { isLoading, doLoad, finsih }
})
