import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import { useLoadingStore } from './loading'
import type { User } from '@/types/User'

export const useUserStore = defineStore('user', () => {
    // Data function
    const loadingStore = useLoadingStore()
    const users = ref<User[]>([])

    async function getUser(id: number) {
        loadingStore.doLoad()
        const res = await userService.getUser(id)
        users.value = res.data
        loadingStore.finsih()
    }
    async function getUsers() {
        loadingStore.doLoad()
        const res = await userService.getUsers()
        users.value = res.data
        loadingStore.finsih()
    }
    async function saveUser(user: User) {
        loadingStore.doLoad()
        if (user.id < 0) { //Add new
            const res = await userService.addUser(user)
        } else { // Update
            const res = await userService.updateUser(user)
        }
        await getUsers()
        loadingStore.finsih()
    }
    async function deleteUser(user: User) {
        loadingStore.doLoad()
        const res = await userService.delUser(user)
        await getUsers()
        loadingStore.finsih()
    }

    return { users, getUsers, saveUser, deleteUser, getUser }
})
